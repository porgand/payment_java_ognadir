package com.ridango.payment.models;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Payment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;
    @NotNull
    private long SenderAccountId;
    @NotNull
    private long ReceiverAccountId;
    @NotNull
    private BigDecimal Amount;
    private long Timestamp;
    public Payment(long senderAccountId, long receiverAccountId, BigDecimal amount){
        this.setSenderAccountId(senderAccountId);
        this.setReceiverAccountId(receiverAccountId);
        this.setAmount(amount);
    }
}
