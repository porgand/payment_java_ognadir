package com.ridango.payment.models;


import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Getter
@Setter
@Entity
@NoArgsConstructor
public class Account  implements Serializable {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;

    @Column(length = 150, nullable = false)
    private String Name;

    @NotNull
    private BigDecimal Balance;

    public Account(String name, BigDecimal balance){
        this.Name = name;
        this.Balance = balance;
    }
}
