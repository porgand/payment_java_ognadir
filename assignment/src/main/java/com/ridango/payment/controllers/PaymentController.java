package com.ridango.payment.controllers;

import com.ridango.payment.models.Payment;
import com.ridango.payment.services.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@RestController
@RequestMapping(value = "/payment", produces = { "application/json" })
public class PaymentController {
    private static final Logger log = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    PaymentService paymentService;

    @GetMapping
    public ResponseEntity<List<Payment>> index() {
        return new ResponseEntity(paymentService.readAllPayments(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Payment> addPayment(@RequestBody Payment payment) {
        log.info("POST PAYMENT request body:  " + payment);
        if(payment.getAmount().compareTo(BigDecimal.valueOf(0)) == 0 ||
                payment.getSenderAccountId() == 0 || payment.getReceiverAccountId() == 0){
            return new ResponseEntity(null, HttpStatus.CONFLICT);
        }

        Payment p = paymentService.addPayment(payment);
        return new ResponseEntity<>(p, p != null ? HttpStatus.CREATED : HttpStatus.NOT_ACCEPTABLE);
    }

}
