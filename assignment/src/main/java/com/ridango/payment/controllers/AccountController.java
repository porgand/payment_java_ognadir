package com.ridango.payment.controllers;

import com.ridango.payment.models.Account;
import com.ridango.payment.services.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/account")
public class AccountController {
    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    AccountService accountService;

    @GetMapping
    public ResponseEntity<List<Account>> index(){
        return new ResponseEntity(accountService.readAllAccounts(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Account> getById(@PathVariable long id){
        return new ResponseEntity(accountService.getAccountsById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Account> createAccount(@RequestBody Account account){
        log.info("CREATE ACCOUNT request body:  " + account + "name is null ? " + (account.getName() == null));
        if(account.getName() == null || account.getName().trim().isEmpty() ||
            account.getBalance() == null){
            return new ResponseEntity(null, HttpStatus.CONFLICT);
        }
        Account newAccount = accountService.createAccount(account);
        return new ResponseEntity(newAccount, newAccount != null? HttpStatus.CREATED : HttpStatus.NOT_ACCEPTABLE);
    }
}
