package com.ridango.payment;

import com.ridango.payment.models.Account;
import com.ridango.payment.repositories.AccountRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.math.BigDecimal;

@Configuration
public class PaymentAppConfiguration {

    @Bean
    public FilterRegistrationBean<CorsFilter> corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }

    @Bean
    public CommandLineRunner mappingDemo(AccountRepository accountRepository) {
        return args -> {
            // create accounts
            Account senderAccount = new Account("Veera Klaara", BigDecimal.valueOf(234.33));
            Account receiverAccount = new Account("Kalle Kallas", BigDecimal.valueOf(10));
            accountRepository.save(senderAccount);
            accountRepository.save(receiverAccount);
        };
    }
}
