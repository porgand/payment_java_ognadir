package com.ridango.payment.services;

import com.ridango.payment.models.Payment;
import com.ridango.payment.repositories.PaymentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PaymentService {
    private static final Logger log = LoggerFactory.getLogger(PaymentService.class);

    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    AccountService accountService;

    public List<Payment> readAllPayments (){
        return paymentRepository.findAll();
    }

    public Payment addPayment(Payment pm) {
        log.info("POST PAYMENT service payment:  " + pm);
        Payment newPayment = null;
        if(accountService.transaction(pm.getSenderAccountId(), pm.getReceiverAccountId(), pm.getAmount())){
            pm.setTimestamp(new Date().getTime());
            newPayment = paymentRepository.save(pm);
        }
        return newPayment;
    }
}
