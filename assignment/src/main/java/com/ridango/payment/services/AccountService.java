package com.ridango.payment.services;

import com.ridango.payment.models.Account;
import com.ridango.payment.repositories.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {
    private static final Logger log = LoggerFactory.getLogger(AccountService.class);
    @Autowired
    AccountRepository accountRepository;

    public List<Account> readAllAccounts (){
        return accountRepository.findAll();
    }

    public Account createAccount(Account a) {
        a.setName(a.getName().trim());
        return accountRepository.save(a);
    }

    public Account getAccountsById(long accountId) {
        Optional a = accountRepository.findById(accountId);
        return a.isPresent()? (Account) a.get() : null;
    }

    public boolean transaction(long senderId, long receiverId, BigDecimal amount) {
        boolean transactionWasSuccessful = false;
        try {
            Optional senderAccount_op = accountRepository.findById(senderId);
            if (senderAccount_op.isPresent()) {
                Account senderAccount = (Account) senderAccount_op.get();
                BigDecimal senderBalance = senderAccount.getBalance();
                if (senderBalance.compareTo(amount) >= 0) {
                    Optional receiverAccount_op = accountRepository.findById(receiverId);
                    if (receiverAccount_op.isPresent()) {
                        Account receiverAccount = (Account) receiverAccount_op.get();
                        senderAccount.setBalance(senderBalance.subtract(amount));
                        accountRepository.save(senderAccount);
                        receiverAccount.setBalance(receiverAccount.getBalance().add(amount));
                        accountRepository.save(receiverAccount);
                        transactionWasSuccessful = true;
                    }
                }
            }
        }
        catch(IllegalArgumentException e){
            log.info("IllegalArgumentException:  " + e.getMessage());
        }
        catch(Exception e){
            log.info("Exception:  " + e.getMessage());
        }

        return transactionWasSuccessful;
    }

}
