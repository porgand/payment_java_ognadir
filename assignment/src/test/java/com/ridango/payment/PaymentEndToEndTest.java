package com.ridango.payment;

import com.ridango.payment.controllers.PaymentController;
import com.ridango.payment.models.Payment;
import com.ridango.payment.services.PaymentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
@AutoConfigureMockMvc
@SpringBootTest(classes = PaymentApplication.class)
@ActiveProfiles("test")
public class PaymentEndToEndTest {
    @Autowired
    private PaymentController controller;
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PaymentService paymentService;

    @Test
    public void contextLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test
    public void postPaymentExceedsBalanceTest() {
        Payment payment = new Payment(1L, 2L, BigDecimal.valueOf(40000));
        given()
                .contentType("application/json")
                .body(payment)
                .when()
                .post("/payment")
                .then()
                .statusCode(406);
    }

    @Test
    public void postPaymentTest() {
        Payment payment = new Payment(1L, 2L, BigDecimal.valueOf(40));

        given()
                .contentType("application/json")
                .body(payment)
                .when()
                .post("/payment")
                .then()
                .statusCode(201);
    }
}
